const express = require("express");
const router = express.Router();
const productController = require("../controller/productController");
const auth = require("../auth")

router.post("/addProduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	if (data.isAdmin){

		productController.addProduct(data).then(resultFromController => res.send(resultFromController));


	} else {
		res.send(false)
	}

});

// Retrieving all products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/", (req, res) => {
productController.getAllActive().then(resultFromController => res.send(resultFromController));
});	


router.get("/:id", (req, res) => {
    productController.getSpecificProduct(req.params.id).then(resultFromController => res.send(resultFromController));
});


router.put("/:productId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));


	} else {
		res.send(false)
	}

});

router.patch("/:productId/archive", auth.verify, (req, res) => {

	
	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});





module.exports = router;