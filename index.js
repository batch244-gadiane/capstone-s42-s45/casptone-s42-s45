const express = require ("express");
const mongoose = require("mongoose");
const app = express();
const cors = require ("cors");
//const PORT = 5000;
const userRoutes = require("./routes/userRoutes");
const productRoutes = require ("./routes/productRoutes");

//Mongodb Connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.wrqeasd.mongodb.net/b244_ecommerce?retryWrites=true&w=majority",

{
	useNewUrlParser : true,
	useUnifiedTopology: true

});

mongoose.connection.once('open', ()=> console.log('Now connected to the MongoDB DataBase'));

app.use (express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen (process.env.PORT || 4000, () =>
 {console.log(`you are connected to my API ${process.env.PORT || 4000}`)
});