const bcrypt = require("bcrypt");
const auth = require("../auth")
const User = require("../model/User");
const Product = require ("../model/Product");



module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

	
		if (result.length > 0) {
			return true;

		} else {
			return false;
		}
	})
};


module.exports.registerUser = (reqBody) =>  {

	
	let newUser = new User ({
		email : reqBody.email,
		firstName :reqBody.firstName,
		lastName : reqBody.lastName,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {

		if (error) {
			return false;

		
		} else {
			return true;
		}
	})
}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

		
			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

module.exports.getAllUser = () => {

	return User.find({}).then(result => {
		return result;
	})
};

module.exports.getProfile =(reqBody) => {
	return User.findById(reqBody.userId).then(result => {

		result.password = "";
		return result;
	});
	
};		

module.exports.productOrder = async (data) => {
 
  let isProductUpdated = await Product.findById(data.productId);
  isProductUpdated.orders.push({ userId: data.userId });

  let isUserUpdated = await User.findById(data.userId);

  let totalAmount = isProductUpdated.price * data.quantity;
  
  let orderDetails = {
    products: {
      productName: isProductUpdated.name,
      productQuantity: data.quantity,
    },
    totalAmount: totalAmount,
  };
  
  isUserUpdated.orders.push(orderDetails);

  if (isUserUpdated && isProductUpdated) {
    await isProductUpdated.save();
    await isUserUpdated.save();
    return true;
  } else {
    return false;
  }
};
