const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
      email: {
        type: String,
        required: [true, "Email Address is required."],
      },
      password: {
        type: String,
        required: [true, "Password is required."],
      },
      isAdmin: {
        type: Boolean,
        default: false,
      },

      orders: [
        {
          products: [
            {
              _id: false,
              productName: {
                type: String,
                required: [true, "Input Product's Name"],
              },
              productQuantity: {
                type: Number,
                required: [true, "Input Product's Quantity"],
              },
            },
          ],

          totalAmount: {
            type: Number,
            required: [true, "Input Product's Amount"],
          },
          purchasedOn: {
            type: Date,
            default: new Date(),
          },
        },
      ],
      });
      module.exports = mongoose.model("User", userSchema);
