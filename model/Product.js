const mongoose = require("mongoose");

const productSchema = new mongoose.Schema ({
		name: {
			type : String,
			required : [true, "Product name is required."]
		},
		description: {
			type : String,
			required : [true, "Description name is required."]
		},
		price: {
			type : Number,	
			required: [true, "Price of the product is required."]
		},
		isActive : {
				type: Boolean,
				default : true
		},
		createdOn : {
			type: Date,
			default : new Date()
		},
		orders : [{
			userId: {
			type: String,
			required: [true, "Order Id is required."]

			}
		}]


});


module.exports = mongoose.model("product", productSchema);